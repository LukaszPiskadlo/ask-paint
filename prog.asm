org 7C00h
	push cs
	pop ds
	
reset:
	mov al, 0
	mov dl, 0	; X pos max 79
	mov dh, 0 	; Y pos max 24
	mov bh, 0
	
	mov ah, 0x0E
	mov ch, 0x10
	int 0x10
		
setposition:
	mov ah, 0x02
	mov al, 0
	int 0x10
		
read:
	mov ah, 0x00
	int 0x16
a:
	cmp al, 0x61
	jnz d
	sub dl, 1
	jmp print
d:
	cmp al, 0x64
	jnz w
	add dl, 1
	jmp print
w:
	cmp al, 0x77
	jnz s
	sub dh, 1
	jmp print
	
s:
	cmp al, 0x73
	jnz ckey
	add dh, 1
	jmp print	

ckey:
	cmp al, 0x63
	jz clearscreen
	
l:
	cmp al, 0x6C
	jnz j
	add dl, 1
	jmp setposition
	
j:
	cmp al, 0x6A
	jnz i
	sub dl, 1
	jmp setposition
	
i:
	cmp al, 0x69
	jnz k
	sub dh, 1
	jmp setposition
	
k:
	cmp al, 0x6B
	jnz backspace
	add dh, 1
	jmp setposition
	
backspace:
	cmp al, 0x08
	jnz print
	
clear:
	mov ah, 0x0E
	mov al, ' '
	int 0x10
	jmp setposition
	
print:
	mov ah, 0x0E
	mov al, [player]
	int 0x10
	jmp setposition
	
clearscreen:
	mov ah, 0x07
	mov al, 0x00
	mov bh, 0x07	; color
	mov cx, 0x00
	mov cx, 0x00
	mov dl, 79
	mov dh, 24
	int 0x10    

	jmp reset
	
player DB '*'
	
TIMES 510-($-$$) DB 0
DW 0xAA55
